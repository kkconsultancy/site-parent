# SITE Parent App -Service#
Project Code: SITE0002

SITE Parent App- Service is a part of SITEParent Application. 
## Use Cases ##

### Use Case Diagram ###
![usecases.png](https://bitbucket.org/repo/qEdkgrG/images/1924135576-usecases.png)

## User Stories ##
### SITE002-001: LOGIN ###

* Actor: Parent 
* Pre Condition:

1. User should have a working PHONE NUMBER which is obtained in database.

* Input:

1. Parent Phone Number.

* Sequence:

1. User opens SiteParent App and click on 'SUBMIT'.
2. Redirects to OTP page appears when valid phone number.
3. User supplies the OTP sent to his/her phone number.
4. When entered OTP is correct user loggined sucessfully and      opens Profile page.
5. It redirects to Home page displays buttons like View profile,View Marks,View Attendance,View Achievements,View Fee,Suggestions,Share and Logout.

* Post Condition:

1. Page displays logggined successfully message and the profile page appears.

* Exceptions:

1. Displays Prompt when the number is not obtained in database if parent enters invalid phone number and stays in login page until enter valid phone number.
3. OTP page shows "resend" button, in case the OTP code is not entered in the first 5 minutes after the OTP has been sent.
4. OTP page shows "cancel" button ,in case user wants to redirect login page or exit from Application.

### SITE0002-002: View Profile with SITEParent APP ID ###

* Actor: Parent 

* Pre Condition:

1. User should have a Loggined Sucessfully with valid Phone number.

* Input:

1. Parent Phone number.

* Sequence:

1. User clicks the ViewProfile button in home page. 
2. Profile page appears.
3. Page displays student details along with parent name and Phone number.

* Post Condition:

1. Page displays Student deatails to viewed by parent.

* Exceptions:

1. Login page re-appears with appropriate message if user enters invalid Phone number.
2. Prompt box re-appears with appropriate message if user phone number is not present in database.
3. Login page re-appears with appropriate message if user fails to enter valid OTP Code within 5 minutes.

### SITE0002-003: View Marks with SITE Parent APP ID###

* Actor: Parent 

* Pre Condition:

1. User should have a Loggined Sucessfully with valid Phone number.


* Input:

1. Phone number.

* Sequence:
 
1. User click on View Marks button.
2. User Marks page appears.
3. User click selective buttons like External Marks,Mid Marks,Laboratory Marks,Class Tests Marks,Assignment and Quiz Marks.
4. Displays student Mark details and Aggregated percentage according to select Marks button.

* Post Condition:

1. User View Marks page appears with name of the user displayed.

* Exceptions:

1. Login page re-appears with appropriate message if user enters invalid Phone number.

### SITE0002-004 View Attendance with SITE Parent App ID###

* Actor: Parent 
 
* Pre Condition:

1. User should have a Loggined Sucessfully with valid Phone number.

* Input:
1. Parent Phone number.

* Sequence:

1. User opens Home page.
2. User clicks on View Attendance button.
3. User View Student Attendace with name of the student displayed.
4. Page displays Student Attendace Percentage.
5. User view student absent information according to year,month and Day.
6. Page displays Blue circle to indicate Student Present on that day.
7. Page displays Red circle to indicate Student Absent on that day.
8. Page displays Orange circle to indicate Student is not Present full day.
9. Page displays Number of periods absent according to the class subjects.

* Post Condition:

1. User should be able to redirect to home Page

* Exceptions:

1. Login page re-appears with appropriate message if user enters invalid Phone number.
2. Prompt box re-appears with appropriate message if user enters invalid OTP code.

### SITE0002-005  View Achievements with SITE Parent App ID###

* Actor: Paren 

* Pre Condition:

1. User should have a Loggined Sucessfully with valid Phone number.

* Input:

1. Parent Phone number.

* Sequence:

1. User click on View Achievements button in Home Page.
2. Page displays the Student Achievements.
3. Page displays 'Certifications and Awards'.
4. User click on Certifications .
5. Page displays student certifications.
6. User click on Awards.
7. page displays student Awards. 

* Post Condition:

1. User should be able to redirect to home Page

* Exceptions:

1. Login page re-appears with appropriate message if user enters invalid Phone number.
2. Prompt box re-appears with appropriate message if user enters invalid OTP code.


### SITE0002-006 View Fee with SITE Parent App ID ###

* Actor: Parent

* Pre Condition:

1. User should have a Loggined Sucessfully with valid Phone number.

* Input:

1. Parent Phone number.

* Sequence:

1. User click on View Fee button on home Page.
2. Page displays selective buttons Total Fee, Till now paid Amount,DUE Amount and notifications.                                                                         3. User click on Total Fee.
4. Displays Total amount to be paid to the student.
5. User click on Till now paid.
6. Displays the total amount paid by the parent to the student so far.
7. User clicks on DUE amount.
8. Displays the due amount along with dates to the user.
9.Displays the notifications to remaind fee to the parent. 

* Post Condition:

1. User should be able to redirect to home Page

* Exceptions:

1. Login page re-appears with appropriate message if user enters invalid Phone number.
2. Prompt box re-appears with appropriate message if user enters invalid OTP code.


### SITE0002-007 Suggestions with SITE Parent App ID ###

* Actor: Parent

* Pre Condition:

1. User should have a Loggined Sucessfully with valid Phone number.

* Input:

1. Parent Phone number.

* Sequence:

1. User click on Suggestions button.
2. Page displays Rating Status and Feedback. 
3. User enters/skips feedback.
4. User click on 'Submit' to Rating and Feedback
5. User click on 'cancel' to Redirect Home page.

* Post Condition:

1. User should be able to redirect to home Page

* Exceptions:

1. Login page re-appears with appropriate message if user enters invalid Phone number.
2. Prompt box re-appears with appropriate message if user enters invalid OTP code.

### SITE0002-008 Share with SITE Parent App ID ###

* Actor: Parent

* Pre Condition:

1. User should have a Loggined Sucessfully with valid Phone number.

* Input:

1. Parent Phone number.

* Sequence:

1. User click on Share button.
2. Page displays apps whatsapp,facebook other social media apps. 
3. User enters/skips share to promote.
4. User click on 'Submit' to Share.
5. User click on 'cancel' to Redirect Home page.

* Post Condition:

1. User should be able to redirect to home Page

* Exceptions:

1. Login page re-appears with appropriate message if user enters invalid Phone number.
2. Prompt box re-appears with appropriate message if user enters invalid OTP code.

### SITE0002-009 Logoutn with SITE Parent App ID ###

* Actor: Parent

* Pre Condition:

1. User should have a Loggined Sucessfully with valid Phone number.

* Input:

1. Parent Phone number.

* Sequence:

1. User click on Logout button.
2. Page displays prompt succesfully logged out.
3. Page displays login page after logout. 
3. User enters/skips Logout.

* Post Condition:

1. User should be able to redirect to home Page

* Exceptions:

1. Login page re-appears with appropriate message if user enters invalid Phone number.
2. Prompt box re-appears with appropriate message if user enters invalid OTP code.

## Workflow ##

### Workflow Diagram ###
![Workflow.png](https://bitbucket.org/repo/qEdkgrG/images/580717476-Workflow.png)

## Development ##

### Dependencies ###
1. Android Studio. 
2. Visual Studio Code

### How to run ###
1. Move to the 'app' folder
```
cd app
```
2. Install the dependencies
```
npm install
``` 
3. Run the app
```
npm start
```
4. Verify http://localhost:8081